﻿using UnityEngine;

[CreateAssetMenu(menuName = "Items/item")]
public class PerceptronTest : ScriptableObject
{

    [SerializeField] private double Sourness;
    public double Input1 => Sourness;

    [SerializeField] private double Sweetness;
    public double Input2 => Sweetness;

    [SerializeField] private double output;
    public double Output => output;

#if UNITY_EDITOR
    private void OnValidate()
    {
        if (Sourness > 1 || Sourness < -1)
            Sourness = 0;
        if (Sweetness > 1 || Sweetness < -1)
            Sweetness = 0;
        if (output > 1 || output < -1)
            output = 0;

    }
#endif

}
