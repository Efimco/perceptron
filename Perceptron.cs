﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Random = System.Random;

public class Perceptron : MonoBehaviour
{
    [SerializeField] private List<PerceptronTest> perceptronTests = new List<PerceptronTest>();
    Random rnd = new Random();
    
    private double[] weights = { 0, 0 };
    private double bias;
    private string pathToFile;
    private string fileName = "Data.txt";
    private string fullPath;
    private char separator = ':';
    public Inventory inv;

    private void Awake()
    {
        pathToFile = Application.persistentDataPath;
        fullPath = Path.Combine(pathToFile, fileName);
        for (int i = 0; i < weights.Length; i++)
        {
            weights[i] = GetRandomDouble(-1, 1);
        }
        bias = GetRandomDouble(-1, 1);
    }
    void Start()
    {
        if (File.Exists(fullPath))
        {
            Load();
        }
        else
            Learn();


    }

    private void Learn()
    {
        double error = 0;
        do
        {
            error = 0;
            foreach (var item in perceptronTests)
            {
                int output = CalculateOutput(item.Input1, item.Input2, bias, weights);
                double localError = item.Output - output;

                weights[0] = item.Input1 * localError + weights[0];
                weights[1] = item.Input2 * localError + weights[1];

                bias += localError;
                error += System.Math.Abs(localError);
                Debug.Log($"W1 {weights[0]}, W2 {weights[1]}, Bias {bias}");
                Debug.Log($"Output {output}");
                Debug.Log($"Error {error}");
                if (error < 0.5)
                {
                    if (output == 1)
                    {
                        inv.Vegatables.Add(item);
                    }
                    else
                    {
                        inv.Fruits.Add(item);
                    }

                }
            }

        } while (error > 0.5d);
        Save();
    }

    private int CalculateOutput(double input1, double input2, double bais, double[] weights)
    {
        double result = input1 * weights[0] + input2 * weights[1] + bais;
        return (result >= 0) ? 1 : 0;
    }

    private double GetRandomDouble(double min, double max)
    {
        return rnd.NextDouble() * (max - min) + min;
    }
    private void Save()
    {

        File.WriteAllText(fullPath, weights[0].ToString() + separator + weights[1].ToString());
    }

    private void Load()
    {
        string Weights = File.ReadAllText(fullPath);
        string[] data;
        data = Weights.Split(separator);
        weights[0] = double.Parse(data[0]);
        weights[1] = double.Parse(data[1]);

    }
}
